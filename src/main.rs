extern crate piston_window;

use piston_window::{PistonWindow, WindowSettings, Transformed, Glyphs, Graphics};
use piston_window::{Event, Input, UpdateArgs, Button, Key};
use piston_window::{Texture, Flip, TextureSettings, Image};
use piston_window::{clear, rectangle, text, image};


fn main() {
    let mut window: PistonWindow = WindowSettings::new("piston-tutorial", (600, 600))
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mut glyphs = Glyphs::new("OpenSans-Regular.ttf", window.factory.clone())
        .unwrap();

    let tiles = Texture::from_path(&mut window.factory, "tiles.png",
                                   Flip::None, &TextureSettings::new())
        .unwrap();

    let mut rotation: f64 = 0.0;

    while let Some(e) = window.next() {
        // http://docs.piston.rs/piston_window/input/enum.Event.html
        match e {
            Event::Update(UpdateArgs{dt}) => {
                rotation += 3.0 * dt;
            }
            // Input handling: http://docs.piston.rs/piston_window/input/enum.Input.html
            Event::Input(Input::Press(Button::Keyboard(Key::Q))) => {
                break;
            }
            _ => {
                println!("{:?}", e);
            }
        }

        // The `c` here is: http://docs.piston.rs/piston_window/graphics/context/struct.Context.html
        // The `g` implements: http://docs.piston.rs/piston_window/graphics/trait.Graphics.html
        window.draw_2d(&e, |c, g| {
            clear([0.17, 0.52, 0.38, 1.0], g);

            let imgtransform = c.transform.trans(16.0, 60.0);
            // Draw a view into an image (e.g. a tile from a tileset):
            // NOTE: image.draw(...) and g.image(...) are equivalest
            Image::new().src_rect([0, 0, 64, 32]).draw(&tiles, &c.draw_state, imgtransform, g);
            g.image(&Image::new().src_rect([64, 0, 32, 32]),
                    &tiles, &c.draw_state, imgtransform.trans(64.0, 0.0));

            // Draw the full image:
            image(&tiles, imgtransform.trans(64.0, 256.0), g);

            // Draw a list of images. Can be tiles from a tileset.
            // The first argument is a slice of (Rectangle, SourceRectangle).
            // They both are: [x, y, width, height], but their meaning differ.
            // The Rectangle is the position of the rendered point and its width and height.
            // The SourceRectangle is the position inside the tilemap and its width and height.
            // So: we use Rectangle[0, 1] to position the thing on the screen and SourceRectangle[0, 1] to find out which slice to take.
            // The width and height should pretty much stay identical in all cases.
            image::draw_many(&[([0.0, 0.0, 32.0, 32.0], [64, 32, 32, 32]),
                               ([32.0, 32.0, 32.0, 32.0], [128, 64, 32, 32])],
                             [1.0, 1.0, 1.0, 1.0],
                             &tiles,
                             &c.draw_state,
                             imgtransform.trans(64.0, 64.0),
                             g,
            );

            // Draw some text at multiple sizes
            text([1.0, 1.0, 1.0, 1.0], 12, "Whoa! A spinning square!",
                 &mut glyphs, c.transform.trans(20.0, 20.0), g);
            text([1.0, 1.0, 1.0, 1.0], 22, "Whoa! A spinning square!",
                 &mut glyphs, c.transform.trans(20.0, 40.0), g);
            text([1.0, 1.0, 1.0, 1.0], 32, "Whoa! A spinning square!",
                 &mut glyphs, c.transform.trans(20.0, 70.0), g);

            let center = c.transform.trans(300.0, 300.0);
            let square = rectangle::square(0.0, 0.0, 100.0);
            let red = [1.0, 0.0, 0.0, 1.0];
            rectangle(red, square, center.rot_rad(rotation).trans(-50.0, -50.0), g); // We translate the rectangle slightly so that it's centered; otherwise only the top left corner would be centered

        });
    }
}
