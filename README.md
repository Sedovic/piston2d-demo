This is a little demo using the Piston game engine and more
specifically, the `piston_window` crate.

I've struggled to find a good docs on how to get started, but I got
this to work and it should be enough for doing quick prototypes for 2D
games (3D should work, too).

It doesn't look particularly nice and I've no idea about the
performance. But whatever.


Here's what we do here:

* Render a graphical shape (square)
* Render a full image
* Render parts of that image (tiles from a tileset)
* Fonts!! (although the glyph rendering looks **terrible**)
* Translation and rotation
* Keyboard input handling


Basically, you take what's here and you can build yourself a game.

Also, and as far as I can tell, no external dynamic libraries (such as
SDL or freetype), so you just clone and `cargo run` it. And I guess
that means it could be possible to cross-compile this maybe?


What's missing and would be cool to have:

* Fullscreen support (plus borderless windows)
* Mouse input
* Sound
* Controller input
